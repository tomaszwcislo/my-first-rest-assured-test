The scenario is made for https://fakerestapi.azurewebsites.net/swagger/ui/index#/ following by Authors requests.
It includes 7 request tests with successful ends:

1. GET for /authors/books/{idBook} 

       a/ where the idBook is 1
       b/ assert that the status code is 200 OK
       c/ assert that the body "ID[0]" = 1
       d/ assert that the body "LastName[0]" = "Last Name 1"

2. GET with extraction of body to class for /authors/books/{idBook}

       a/ where the idBook is 1
       b/ the body extraction to Author.class
       c/ assert that authors is not empty 

3. GET for /api/Authors

       a/ show all authors
       b/ assert that the status code is 200 OK
       c/ assert that body "IDBook" = 1
       d/ assert that body "FirstName[2]" = "First Name 3"
       e/ assert that body "ID" has 399 items

4. GET for /api/Authors/{id}

       a/ where the author id is 0
       b/ assert that the status code is 404 Not Found - because author with id 0 does not exist
       c/ assert that content type is empty or null string
       
5. POST for /api/Authors

       a/ put a new author where:
          "ID" = 0, "IDBook" = 0, "FirstName" = "String", "LastName" = "String";
       b/ assert that the status code is 200 OK
       c/ assert that the body response looks:
          {
            "ID": 0,
            "IDBook": 0,
            "FirstName": "String",
            "LastName": "String"
          }
       
6. PUT for /api/Authors/{id}

       a/ update data for author "ID" = 0, where:
          "ID" = 0, "IDBook" = 0, "FirstName" = "Rocky", "LastName" = "Balboa";
       b/ assert that the status code is 200 OK
       c/ assert that the body response looks:
          {
            "ID": 0,
            "IDBook": 0,
            "FirstName": "Rocky",
            "LastName": "Balboa"
          }
       
7. DELETE for /api/Authors/{id}

       a/ where the author id is 1
       b/ assert that the status code is 200 OK
       c/ assert that content type is empty or null string
       