import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;

public class BaseTestClass {

    RequestSpecification requestSpec;

    @BeforeMethod
    public void setup() {

        RestAssured.baseURI = "https://fakerestapi.azurewebsites.net";

        requestSpec = new RequestSpecBuilder()
                .addHeader("Content-Type", "application/json")
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
    }
}

