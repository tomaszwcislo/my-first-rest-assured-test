import org.testng.annotations.Test;
import utilities.Author;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;


public class FirstRestAssuredTest extends BaseTestClass {


    @Test
    void GET_authors_books_idBook() {

        given()
                .get("/authors/books/1")
        .then()
                .assertThat()
                    .statusCode(200)
                    .body("ID[0]", equalTo(1))
                    .body("LastName[0]", is("Last Name 1"))
                .log().all();
    }

    @Test
    void GET_extractToAuthorClass() {

        List<Author> authors =
        given()
                .get("/authors/books/1")
        .then()
                .log()
                    .body()
                .extract()
                    .body()
                .jsonPath().getList(".", Author.class);
        assertThat(authors).isNotEmpty();
    }

    @Test
    void GET_api_authors() {

        given()
                .get("/api/Authors")
        .then()
                .assertThat()
                    .statusCode(200)
                    .body("IDBook[0]", equalTo(1))
                    .body("FirstName[2]", equalTo("First Name 3"))
                    .body("ID", hasItems(399))
                .log().all();
    }

    @Test
    void GET_api_authors_id() {

        given()
                .get("/api/Authors/0")
        .then()
                .assertThat()
                    .statusCode(404)
                    .contentType(is(emptyOrNullString()))
                .log().all();
    }

    @Test
    void POST_api_authors() {

        Author author = new Author();
               author.setID("0");
               author.setIDBook("0");
               author.setFirstName("String");
               author.setLastName("String");

        given()
                .spec(requestSpec)
                .body(author)
        .when()
                .post("/api/Authors")
        .then()
                .assertThat()
                    .statusCode(200)
                    .body("ID", equalTo(0))
                    .body("IDBook", equalTo(0))
                    .body("FirstName", is("String"))
                    .body("LastName", is("String"))
                .log().all();
    }

    @Test
    void PUT_api_authors_id() {

        Author author = new Author();
               author.setID("0");
               author.setIDBook("0");
               author.setFirstName("Rocky");
               author.setLastName("Balboa");

        given()
                .spec(requestSpec)
                .body(author)
        .when()
                .put("/api/Authors/0")
        .then()
                .assertThat()
                    .statusCode(200)
                    .body("ID", equalTo(0))
                    .body("IDBook", equalTo(0))
                    .body("FirstName", is("Rocky"))
                    .body("LastName", is("Balboa"))
                .log().all();
    }

    @Test
    void DELETE_api_authors_id() {

        when()
                .delete("/api/Authors/1")
        .then()
                .assertThat()
                    .statusCode(200)
                    .contentType(is(emptyOrNullString()))
                .log().all();
    }
}
